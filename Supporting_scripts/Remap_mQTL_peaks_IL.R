###########################################################################################
### trait transformation checks
###########################################################################################


    ###Re-map C14:1 mQTL using different transformations
        QTL.data <- filter(Metabolomics_normalized_data, !is.na(value),strain_type %in% c("RIL")) %>%
            group_by(strain,metabolite,Data_type,trait_transformation) %>%
            summarise(value=mean(value,na.rm=T)) %>%
            data.frame() %>%
            filter(metabolite=="C14:1") %>%
            select(trait_transformation,metabolite,strain,value) %>%
            spread(key=strain,value=value) %>%
            as.data.frame()
        rownames(QTL.data) <- apply(QTL.data[,1:2],1,paste,collapse="_")            
        QTL.data <- data.matrix(QTL.data[,-c(1:2)]) 
        
        ###Check how many strains have a value
        table(apply(!is.na(QTL.data),1,sum))
        
        QTL.data <- QTL.data[apply(!is.na(QTL.data),1,sum)>50,]
        rownames(QTL.data)
        
        
        ###QTL mapping & permutation
        QTL.data <- QTL.data.prep(QTL.data,strain.trait=colnames(QTL.data),strain.map=popmap,strain.marker=popmrk)
        
        C14_1.QTL <- map.1(trait.matrix=QTL.data[[1]],strain.map=QTL.data[[2]],strain.marker=QTL.data[[3]])
        
        C14_1.peak.QTL <- mapping.to.list(map1.output=C14_1.QTL) %>%
                          peak.finder(threshold=3.7)
        ###Plot QTL profiles
            data.plot <- separate(C14_1.peak.QTL,trait,into=c("transformation","metabolite"),sep="_") %>%
                         mutate(transformation=gsub("Abs.","Absolute",transformation),
                                transformation=gsub("Perc.","Relative",transformation),
                                transformation=gsub("conc","",transformation),
                                transformation=gsub("strain","",transformation),
                                transformation=gsub("batch","\nbatch correction",transformation),
                                transformation=gsub("zscore","\nbatch correction\nz-score",transformation)) %>%
                         filter(!transformation %in% c("Absolute\nbatch correction","Relative\nbatch correction"))
            
            myColors <- brewer.pal(4,"Dark2")
            names(myColors) <- unique(data.plot$transformation)
            
            colqtl <- scale_colour_manual(name = "transformation",values = myColors)
    
            
            ggplot(data.plot,aes(x=qtl_bp,y=qtl_significance,colour=transformation)) +
            geom_line(size=2) + facet_grid(.~qtl_chromosome,scales="free",space="free") + colqtl +
            presentation + scale_x_continuous(breaks=c(5,10,15,20)*10^6,labels=c(5,10,15,20)) + xlab("mQTL location (Mbp)") + ylab("\nsignificance (-log10(p))")

            
            
            
                 
            
    ###Re-map C18:2 mQTL using different transformations
        QTL.data <- filter(Metabolomics_normalized_data, !is.na(value),strain_type %in% c("RIL")) %>%
            group_by(strain,metabolite,Data_type,trait_transformation) %>%
            summarise(value=mean(value,na.rm=T)) %>%
            data.frame() %>%
            filter(metabolite=="C18:2") %>%
            select(trait_transformation,metabolite,strain,value) %>%
            spread(key=strain,value=value) %>%
            as.data.frame()
        rownames(QTL.data) <- apply(QTL.data[,1:2],1,paste,collapse="_")            
        QTL.data <- data.matrix(QTL.data[,-c(1:2)]) 
        
        ###Check how many strains have a value
        table(apply(!is.na(QTL.data),1,sum))
        
        QTL.data <- QTL.data[apply(!is.na(QTL.data),1,sum)>50,]
        rownames(QTL.data)
        
        
        ###QTL mapping & permutation
        QTL.data <- QTL.data.prep(QTL.data,strain.trait=colnames(QTL.data),strain.map=popmap,strain.marker=popmrk)
        
        C18_2.QTL <- map.1(trait.matrix=QTL.data[[1]],strain.map=QTL.data[[2]],strain.marker=QTL.data[[3]])
        
        C18_2.peak.QTL <- mapping.to.list(map1.output=C18_2.QTL) %>%
            peak.finder(threshold=3.7)
        ###Plot QTL profiles
        data.plot <- separate(C18_2.peak.QTL,trait,into=c("transformation","metabolite"),sep="_") %>%
            mutate(transformation=gsub("Abs.","Absolute",transformation),
                   transformation=gsub("Perc.","Relative",transformation),
                   transformation=gsub("conc","",transformation),
                   transformation=gsub("strain","",transformation),
                   transformation=gsub("batch","\nbatch correction",transformation),
                   transformation=gsub("zscore","\nbatch correction\nz-score",transformation)) %>%
            filter(!transformation %in% c("Absolute\nbatch correction","Relative\nbatch correction"))
        
        myColors <- brewer.pal(4,"Dark2")
        names(myColors) <- unique(data.plot$transformation)
        
        colqtl <- scale_colour_manual(name = "transformation",values = myColors)
        
        
        ggplot(data.plot,aes(x=qtl_bp,y=qtl_significance,colour=transformation)) +
        geom_line(size=2) + facet_grid(.~qtl_chromosome,scales="free",space="free") + colqtl +
        presentation + scale_x_continuous(breaks=c(5,10,15,20)*10^6,labels=c(5,10,15,20)) + xlab("mQTL location (Mbp)") + ylab("\nsignificance (-log10(p))")
        

            
            
            
            
            
            
            
                   