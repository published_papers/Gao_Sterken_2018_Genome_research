    
    ###Convenience function for extracting legend from plot
    get_legend<-function(myggplot){
                         tmp <- ggplot_gtable(ggplot_build(myggplot))
                         leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
                         legend <- tmp$grobs[[leg]]
                         return(legend)
                        }

    shapiro.test.dpl <- function(x){
                                    return(shapiro.test(x)$p.value)
                                   }